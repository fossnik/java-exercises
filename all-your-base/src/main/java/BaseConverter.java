import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class BaseConverter {
	private int baseFrom;
	private int[] digits;

	BaseConverter(int _base, int[] _digits) {
		if (_base < 2) {
			throw new IllegalArgumentException("Bases must be at least 2.");
		}
		if (Arrays.stream(_digits).anyMatch(i -> i < 0)) {
			throw new IllegalArgumentException("Digits may not be negative.");
		}
		if (Arrays.stream(_digits).anyMatch(i -> i >= _base)) {
			throw new IllegalArgumentException("All digits must be strictly less than the base.");
		}

		this.baseFrom = _base;
		this.digits = _digits;
	}

	// get essential value from provided representation
	private int convertFromBase() {
		int sum = 0;
		for (int i = digits.length - 1, exp = 0; i >= 0; i--, exp++) {
			sum += digits[i] * Math.pow(baseFrom, exp);
		}

		return sum;
	}

	int[] convertToBase(int baseTo) {
		if (baseTo < 2) {
			throw new IllegalArgumentException("Bases must be at least 2.");
		}

		int value = convertFromBase();

		// find the largest digit
		int largestDigit = 1;
		while (Math.pow(baseTo, largestDigit) < value) {
			largestDigit++;
		}

		List<Integer> valueRepresentation = new ArrayList<>();

		// start from the largest base value (like for number 999),
		// that would be 100, because 999 goes into it 9 times
		for (int i = largestDigit - 1; i >= 0; i--) {
			int baseFactor = (int) Math.pow(baseTo, i);

			if (value / baseFactor >= 0) {
				// how many times does it go into this base factor?
				int timesGoesIn = value / baseFactor;

				value -= baseFactor * timesGoesIn;

				valueRepresentation.add(timesGoesIn);
			}
			else valueRepresentation.add(0);
		}

		return valueRepresentation.stream().mapToInt(Integer::valueOf).toArray();
	}
}