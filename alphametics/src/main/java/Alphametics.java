import java.util.*;

public class Alphametics {
	private String input;

	public Alphametics(String _input) {
		this.input = _input;
	}

	LinkedHashMap<Character, Integer> solve() throws UnsolvablePuzzleException {

		// get all the distinct letters
		Character[] characters = input.chars()
				.mapToObj(c -> (char) c)
				.filter(Character::isAlphabetic)
				.distinct()
				.toArray(Character[]::new);

		// there are only 10 digits - must only be 10 unique characters
		assert characters.length <= 10;

		// calculate maximum number of combinations with no repeating digits
		int possibilities = 10;
		for (int i = 1, m = 9; i < characters.length; i++, m--) {
			possibilities *= m;
		}

		HashSet<String> testedMappings = new HashSet<>();
		LinkedHashMap<Character, Integer> mapping;

		int num;
		do {
			do {
				mapping = new LinkedHashMap<>();
				for (char c : characters) {

					do {
						num = (int) (Math.random() * 10);
					} while (mapping.values().contains(num));

					mapping.put(c, num);
				}

			} while (testedMappings.contains(mapping.toString()));

			testedMappings.add(mapping.toString());

			if (testedMappings.size() == possibilities) {
				throw new UnsolvablePuzzleException("Insolvent");
			}

		} while (!testMapping(mapping));

		return mapping;

//		for (int l9 = 0; l9 <= (ps > 9 ? 9 : 0); l9++) {
//			if (ps >= 10) mapping.put(characters[9], l9);
//			for (int l8 = 0; l8 <= (ps > 8 ? 9 : 0); l8++) {
//				if (ps >= 9) mapping.put(characters[8], l8);
//				for (int l7 = 0; l7 <= (ps > 7 ? 9 : 0); l7++) {
//					if (ps >= 8) mapping.put(characters[7], l7);
//					for (int l6 = 0; l6 <= (ps > 6 ? 9 : 0); l6++) {
//						if (ps >= 7) mapping.put(characters[6], l6);
//						for (int l5 = 0; l5 <= (ps > 5 ? 9 : 0); l5++) {
//							if (ps >= 6) mapping.put(characters[5], l5);
//							for (int l4 = 0; l4 <= (ps > 4 ? 9 : 0); l4++) {
//								if (ps >= 5) mapping.put(characters[4], l4);
//								for (int l3 = 0; l3 <= (ps > 3 ? 9 : 0); l3++) {
//									if (ps >= 4) mapping.put(characters[3], l3);
//									for (int l2 = 0; l2 <= (ps > 2 ? 9 : 0); l2++) {
//										if (ps >= 3) mapping.put(characters[2], l2);
//										for (int l1 = 0; l1 <= (ps > 1 ? 9 : 0); l1++) {
//											if (ps >= 2) mapping.put(characters[1], l1);
//											for (int l0 = 0; l0 <= (ps > 0 ? 9 : 0); l0++) {
//												if (ps >= 1) mapping.put(characters[0], l0);
//
//												// don't bother testing unless each mapping is unique
//												if (mapping.size() == Arrays.stream(mapping.values().toArray()).distinct().count()) {
//													if (testMapping(mapping)) return mapping;
//												}
//											}
//										}
//									}
//								}
//							}
//						}
//					}
//				}
//			}
//		}

//		throw new UnsolvablePuzzleException("Insolvent");
	}

	private boolean testMapping(HashMap<Character, Integer> mapping) {
		String numberString = input;

		// replace each letter with a number
		for (Iterator<Character> iterator = mapping.keySet().iterator(); iterator.hasNext(); ) {
			Character c = iterator.next();
			numberString = numberString.replaceAll(String.valueOf(c), String.valueOf(mapping.get(c)));
		}

		String operands = numberString.split(" == ")[0];
		String answer   = numberString.split(" == ")[1];

		try {
			int sumOfOperands = Arrays
					.stream(operands.split(" \\+ "))
					.mapToInt(Integer::parseInt)
					.reduce(Integer::sum)
					.getAsInt();


			return sumOfOperands == Integer.parseInt(answer)
					&& !answer.startsWith("0")
					&& Arrays.stream(operands.split(" "))
					.noneMatch(x -> x.startsWith("0"));
		} catch (NumberFormatException e) {
			// too large of a number encountered
			long longSumOfOperands = Arrays.stream(operands.split(" \\+ ")).mapToLong(Long::parseLong).sum();

			boolean it = longSumOfOperands == Long.parseLong(answer)
					&& !answer.startsWith("0")
					&& Arrays.stream(operands.split(" "))
					.noneMatch(x -> x.startsWith("0"));

			return it;
		}
	}
}
