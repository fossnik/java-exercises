class UnsolvablePuzzleException extends Exception {
	public UnsolvablePuzzleException(String s)
	{
		super(s);
	}
}
