import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class Anagram {

	private String testWord;

	// frequency of letters in the test word
	private Map<Character, Integer> testwordCharTally = new HashMap<Character, Integer>();

	public Anagram(String input) {
		testWord = input;
		testwordCharTally = frequencyAnalysis(input);
	}

	public List<String> match(List<String> words) {

		List<String> anagrams = new ArrayList<String>();

		// two words with same frequency are anagrams if they are not identical
		for (String word : words)
			if (!testWord.toLowerCase().equals(word.toLowerCase()) &&
					testwordCharTally.equals(frequencyAnalysis(word)))
				anagrams.add(word);

		return anagrams;
	}

	private Map<Character, Integer> frequencyAnalysis(String word) {

		Map<Character, Integer> characterTally = new HashMap<Character, Integer>();

		// count of instances of each letter
		for (char c : word.toLowerCase().toCharArray())
			characterTally.put(c, characterTally.get(c) == null ? 1 : characterTally.get(c) + 1);

		return characterTally;
	}
}