public class BankAccount {

	private int balance;
	private boolean isOpen = false;
	private Object lock = new Object();

	public void open() {
		balance = 0;
		isOpen = true;
	}

	public void close() {
		isOpen = false;
	}

	public void deposit(int value) throws BankAccountActionInvalidException {
		sanityTest(value);

		synchronized(lock) {
			balance += value;
		}
	}

	public void withdraw(int value) throws BankAccountActionInvalidException {
		sanityTest(value);

		if (balance == 0)
			throw new BankAccountActionInvalidException("Cannot withdraw money from an empty account");

		if (value > balance)
			throw new BankAccountActionInvalidException("Cannot withdraw more money than is currently in the account");

		synchronized(lock) {
			balance -= value;
		}
	}

	public int getBalance() throws BankAccountActionInvalidException {
		sanityTest(0);

		return balance;
	}

	private void sanityTest(int value) throws BankAccountActionInvalidException {
		if (!isOpen)
			throw new BankAccountActionInvalidException("Account closed");

		if (value < 0)
			throw new BankAccountActionInvalidException("Cannot deposit or withdraw negative amount");
	}

}