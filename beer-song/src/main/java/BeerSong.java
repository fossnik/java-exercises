public class BeerSong {

	public String sing(int start, int verses) {
		String remit = "";
		int i = start;
		int end = start - verses;
		while (i-- > end && i > 1)
			remit += String.format("%d bottles of beer on the wall, %d bottles of beer.\n" +
					"Take one down and pass it around, %d bottles of beer on the wall.\n\n", i+1, i+1, i);

		if (i > end - 2)
			while(i-- > end - 1)
				remit += nonGenericVerse(i + 2);

		return remit;
	}

	public String singSong() {
		return sing(99, 99) + nonGenericVerse(0);
	}

	private String nonGenericVerse(int v) {
		switch(v) {
			case 2:
				return "2 bottles of beer on the wall, 2 bottles of beer.\n" +
						"Take one down and pass it around, 1 bottle of beer on the wall.\n\n";

			case 1:
				return "1 bottle of beer on the wall, 1 bottle of beer.\n" +
						"Take it down and pass it around, no more bottles of beer on the wall.\n\n";

			case 0:
				return "No more bottles of beer on the wall, no more bottles of beer.\n" +
						"Go to the store and buy some more, 99 bottles of beer on the wall.\n\n";
		}
		return null;
	}
}