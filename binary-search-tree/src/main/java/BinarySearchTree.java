import java.util.*;
import java.util.stream.Collectors;

class BinarySearchTree<T extends Comparable<T>> {
	private static Node root = new Node();

	void insert(T value) {
		// create the initial Node if needed
		if (root.data == null) root.data = value;

		else insert(value, root);
	}

	// recursive insertion
	private void insert(T value, Node nodeOfReference) {

		// value greater than nodeOfReference
		if (value.compareTo((T) nodeOfReference.getData()) > 0) {

			// create node if necessary
			if (nodeOfReference.getRight() == null) {
				nodeOfReference.right = new Node(value);
			}

			else insert(value, nodeOfReference.getRight());
		}

		// value lesser than nodeOfReference
		else if (value.compareTo((T) nodeOfReference.getData()) < 0) {

			// create node if necessary
			if (nodeOfReference.getLeft() == null) {
				nodeOfReference.left = new Node(value);
			}

			else insert(value, nodeOfReference.getLeft());
		}

		// base case - found point of insertion
		else {
			if (nodeOfReference.getLeft() == null) {
				nodeOfReference.left = new Node(value);
			} else {
				// shift down for duplicates
				// create new node and point it to the old one
				Node temp = nodeOfReference.getLeft();
				nodeOfReference = new Node(value);
				nodeOfReference.left = temp;
			}
		}
	}

	List<T> getAsSortedList() {
		return getAsLevelOrderList().stream().sorted().collect(Collectors.toList());
	}

	List<T> getAsLevelOrderList() {
		buildLevelOrderList(root, 0);
		return list.values().stream().flatMap(List::stream).collect(Collectors.toList());
	}

	private SortedMap<Integer, List<T>> list = new TreeMap<>();

	private void buildLevelOrderList(Node node, int level) {
		List<T> revisedEntry = new LinkedList<>(list.getOrDefault(level, new LinkedList<>(Collections.emptyList())));
		revisedEntry.add((T) node.getData());
		list.put(level, revisedEntry);

		if (node.getLeft()  != null) buildLevelOrderList(node.left,  level + 1);
		if (node.getRight() != null) buildLevelOrderList(node.right, level + 1);
	}

	Node<T> getRoot() {
		return root;
	}

	static class Node<T> {
		private T data;
		private Node left, right;

		Node() {
			data = null;
			left = null;
			right = null;
		}

		Node(T data) {
			this.data = data;
		}

		Node<T> getLeft() {
			return left;
		}

		Node<T> getRight() {
			return right;
		}

		T getData() {
			return data;
		}
	}
}
