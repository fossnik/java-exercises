import java.util.List;

class BinarySearch {

	private List<Integer> numberList;

	BinarySearch(List<Integer> list) {
		numberList = list;
	}

	int indexOf(int numberToFind) {
		if (numberList.size() < 1)
			return -1;

		for (int start = 0, end = numberList.size() - 1, testNum = numberList.get((start + end) / 2);
			 start <= end;
			 testNum = numberList.get((start + end) / 2))

			if (testNum > numberToFind)
				end = (start + end) / 2 - 1;

			else if (testNum < numberToFind)
				start = (start + end) / 2 + 1;

			else
				return (start + end) / 2;

		return -1;
	}

}