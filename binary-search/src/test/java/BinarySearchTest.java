
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BinarySearchTest {

    @Test
    public void findsAValueInAnArrayWithOneElement() {
        List<Integer> listOfUnitLength = Collections.singletonList(6);

        BinarySearch search = new BinarySearch(listOfUnitLength);

        assertEquals(0, search.indexOf(6));
    }

    @Test
    public void findsAValueInTheMiddleOfAnArray() {
        List<Integer> sortedList = Collections.unmodifiableList(
                Arrays.asList(1, 3, 4, 6, 8, 9, 11)
        );

        BinarySearch search = new BinarySearch(sortedList);

        assertEquals(3, search.indexOf(6));
    }

    @Test
    public void findsAValueAtTheBeginningOfAnArray() {
        List<Integer> sortedList = Collections.unmodifiableList(
                Arrays.asList(1, 3, 4, 6, 8, 9, 11)
        );

        BinarySearch search = new BinarySearch(sortedList);

        assertEquals(0, search.indexOf(1));
    }

    @Test
    public void findsAValueAtTheEndOfAnArray() {
        List<Integer> sortedList = Collections.unmodifiableList(
                Arrays.asList(1, 3, 4, 6, 8, 9, 11)
        );

        BinarySearch search = new BinarySearch(sortedList);

        assertEquals(6, search.indexOf(11));
    }

    @Test
    public void findsAValueInAnArrayOfOddLength() {
        List<Integer> sortedListOfOddLength = Collections.unmodifiableList(
                Arrays.asList(1, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 634)
        );

        BinarySearch search = new BinarySearch(sortedListOfOddLength);

        assertEquals(9, search.indexOf(144));
    }

    @Test
    public void findsAValueInAnArrayOfEvenLength() {
        List<Integer> sortedListOfEvenLength = Collections.unmodifiableList(
                Arrays.asList(1, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377)
        );

        BinarySearch search = new BinarySearch(sortedListOfEvenLength);

        assertEquals(5, search.indexOf(21));
    }

    @Test
    public void identifiesThatAValueIsNotFoundInTheArray() {
        List<Integer> sortedList = Collections.unmodifiableList(
                Arrays.asList(1, 3, 4, 6, 8, 9, 11)
        );

        BinarySearch search = new BinarySearch(sortedList);

        assertEquals(-1, search.indexOf(7));
    }

    @Test
    public void aValueSmallerThanTheArraysSmallestValueIsNotFound() {
        List<Integer> sortedList = Collections.unmodifiableList(
                Arrays.asList(1, 3, 4, 6, 8, 9, 11)
        );

        BinarySearch search = new BinarySearch(sortedList);

        assertEquals(-1, search.indexOf(0));
    }

    @Test
    public void aValueLargerThanTheArraysSmallestValueIsNotFound() {
        List<Integer> sortedList = Collections.unmodifiableList(
                Arrays.asList(1, 3, 4, 6, 8, 9, 11)
        );

        BinarySearch search = new BinarySearch(sortedList);

        assertEquals(-1, search.indexOf(13));
    }

    @Test
    public void nothingIsFoundInAnEmptyArray() {
        List<Integer> emptyList = Collections.emptyList();

        BinarySearch search = new BinarySearch(emptyList);

        assertEquals(-1, search.indexOf(1));
    }

    @Test
    public void nothingIsFoundWhenTheLeftAndRightBoundCross() {
        List<Integer> sortedList = Collections.unmodifiableList(
                Arrays.asList(1, 2)
        );

        BinarySearch search = new BinarySearch(sortedList);

        assertEquals(-1, search.indexOf(0));
    }

}
