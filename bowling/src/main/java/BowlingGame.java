public class BowlingGame {

	private int score = 0;
	private int last = 0;
	private int roll = 0;
	private int frame = 0;
	private boolean spare = false;
	private boolean strike = false;
	private int consecutiveStrikes = 0;


	public void roll(int pins) {
		roll++;
		sanityTest(pins);

		switch (roll) {
			case 1:
				if (pins == 10 && frame < 10) { // strike
					strike = true;
					consecutiveStrikes++;
					advanceFrame();
				}
				else if (pins == 10 && frame == 10) { // strike on last frame
					strike = true;
					consecutiveStrikes++;
					advanceFrame();
				}

				last = pins;
				break;

			case 2:
				if (pins + last == 10) { // spare
					spare = true;
				}
				else {
					score += pins + last; // open frame
					spare = false;
				}

				strike = false;
				advanceFrame();
				break;

			default:
				throw new IllegalStateException("Oops");
		}
	}

	private void advanceFrame() {
		frame++;
		roll = 0;
	}

	private void sanityTest(int pins) {
		if (pins < 0)
			throw new IllegalStateException("Negative roll is invalid");
		if (pins + last > 10)
			throw new IllegalStateException("Pin count exceeds pins on the lane");
		if (frame > 10)
			throw new IllegalStateException("Cannot roll after game is over");
	}

	public int score() {
		if (frame < 10)
			throw new IllegalStateException("Score cannot be taken until the end of the game");

		return score;
	}
}