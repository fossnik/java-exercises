public class Clock {
	private int hrs;
	private int min = 0;

	public Clock(int hh, int mm) {
		// put overflow minutes into the hours
		while (mm < 0) {
			hh -= 1;
			mm += 60;
		}
		while (min + mm >= 60) {
			hh += 1;
			mm -= 60;
		}

		// resolve negative values
		this.hrs = (hh % 24 + 24) % 24;
		this.min = (mm % 60 + 60) % 60;
	}

	void add(int minutes) {
		Clock clock = new Clock(this.hrs, this.min + minutes);
		this.hrs = clock.hrs;
		this.min = clock.min;
	}

	@Override
	public boolean equals(Object object) {
		if (object != null && getClass() == object.getClass()) {
			Clock b = (Clock) object;
			return this.min == b.min && this.hrs == b.hrs;
		}
		return false;
	}

	@Override
	public String toString() {
		// pad up to two digits "%-2s"
		return String.format("%2s:%2s", hrs, min).replaceAll(" ", "0");
	}
}
