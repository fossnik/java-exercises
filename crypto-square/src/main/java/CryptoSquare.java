class CryptoSquare {
	private String input;

	CryptoSquare(String input) {
		this.input = input;
	}


	public String getCiphertext() {
		char[] fixedInput = this.input
				.toLowerCase()
				.replaceAll("[^.a-z0-9]", "")
				.replaceAll("[.]", " ")
				.toCharArray();

		int edge = (int) Math.ceil(Math.sqrt(fixedInput.length));

		String[] arrays = new String[edge];

		int i = 0;
		for (char c : fixedInput) {
			if (arrays[i] == null) arrays[i] = String.valueOf(c);
			else arrays[i] += String.valueOf(c);

			i++;
			i %= edge;
		}

		// pad last row
		if (arrays.length > 1 && arrays[edge - 1].length() != edge) {
			arrays[edge - 1] = String.format("%-" + (edge - 1) + "s", arrays[edge - 1]);
		}

		return String.join(" ", arrays);
	}
}