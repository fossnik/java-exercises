class Darts {

	private double xcord, ycord;

	Darts(double x, double y) {
		xcord = x;
		ycord = y;
	}

	int score() {
		double radius = Math.sqrt(xcord * xcord + ycord * ycord);

		if (radius > 10)
			return 0;

		if (radius <= 1)
			return 10;

		if (radius <= 5)
			return 5;

		if (radius <= 10)
			return 1;

		return 0;
	}

}
