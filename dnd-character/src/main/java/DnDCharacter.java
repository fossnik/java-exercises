class DnDCharacter {

	private int Strength;
	private int Dexterity;
	private int Constitution;
	private int Intelligence;
	private int Wisdom;
	private int Charisma;

	public DnDCharacter() {
		Strength = ability();
		Dexterity = ability();
		Constitution = ability();
		Intelligence = ability();
		Wisdom = ability();
		Charisma = ability();
	}

	int ability() {
        return (int) (Math.random() * 16) + 3;
    }

    int modifier(int input) {
		// odd numbers less than 10 need manual round-down
		return input < 10 && input % 2 != 0
				? (input - 10) / 2 - 1
				: (input - 10) / 2;
    }

	public int getStrength() {
		return Strength;
	}

	public int getDexterity() {
		return Dexterity;
	}

	public int getConstitution() {
		return Constitution;
	}

	public int getIntelligence() {
		return Intelligence;
	}

	public int getWisdom() {
		return Wisdom;
	}

	public int getCharisma() {
		return Charisma;
	}

	public int getHitpoints() {
		return modifier(this.Constitution) + 10;
	}
}
