import com.sun.xml.internal.ws.api.model.CheckedException;

import java.io.IOException;
import java.text.ParseException;
import java.util.Optional;

class ErrorHandling {

    void handleErrorByThrowingIllegalArgumentException() {
        throw new IllegalArgumentException();
    }

    void handleErrorByThrowingIllegalArgumentExceptionWithDetailMessage(String message) {
        throw new IllegalArgumentException(message);
    }

    /**
     * checked exceptions have to be caught by a try-catch block, or declared with the "throws" keyword in the method signature.
     * unhandled checked exceptions prevent the code from compiling.
     * by handling the exception, it is possible to recover (avoid program termination)
     *
     * Examples:
         * Exception
         * IOException
         * FileNotFoundException
         * ParseException
         * ClassNotFoundException
         * CloneNotSupportedException
         * InstantiationException
         * InterruptedException
         * NoSuchMethodException
         * NoSuchFieldException
     */
    void handleErrorByThrowingAnyCheckedException() throws IOException {
        throw new IOException();
    }

    void handleErrorByThrowingAnyCheckedExceptionWithDetailMessage(String message) throws Exception {
        throw new Exception(message);
    }

    /**
     * Unchecked exceptions typically occur at runtime, and are not recognized by the compiler.
     * It is appropriate to use an unchecked exception when there is no way to recover from it.
     *
     * Examples:
         * ArrayIndexOutOfBoundsException
         * ClassCastException
         * IllegalArgumentException
         * IllegalStateException
         * NullPointerException
         * NumberFormatException
         * AssertionError
         * ExceptionInInitializerError
         * StackOverflowError
         * NoClassDefFoundError
     */
    void handleErrorByThrowingAnyUncheckedException() {
        throw new IllegalArgumentException();
    }

    void handleErrorByThrowingAnyUncheckedExceptionWithDetailMessage(String message) {
        throw new RuntimeException(message);
    }

    /**
     * It is possible to create custom exceptions (from the Exception class) using inheritance.
     */
    void handleErrorByThrowingCustomCheckedException() throws CustomCheckedException {
        throw new CustomCheckedException();
    }

    void handleErrorByThrowingCustomCheckedExceptionWithDetailMessage(String message) throws CustomCheckedException {
        throw new CustomCheckedException(message);
    }

    void handleErrorByThrowingCustomUncheckedException() throws CustomUncheckedException {
        throw new CustomUncheckedException();
    }

    void handleErrorByThrowingCustomUncheckedExceptionWithDetailMessage(String message) throws CustomUncheckedException {
        throw new CustomUncheckedException(message);
    }

    Optional<Integer> handleErrorByReturningOptionalInstance(String integer) {
        try {
            return Optional.of(Integer.parseInt(integer));
        }
        catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

}
