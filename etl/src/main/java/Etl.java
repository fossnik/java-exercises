import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Etl {
    Map<String, Integer> transform(Map<Integer, List<String>> old) {
		Map<String, Integer> remit = new HashMap<>();

		old.forEach((pointValue, letters) ->
				letters.forEach((letter) ->
						remit.put(letter.toLowerCase(), pointValue)));

		return remit;
    }
}
