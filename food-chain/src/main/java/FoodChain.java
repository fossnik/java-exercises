public class FoodChain {

	private static final String[] chain =  {"fly", "spider", "bird", "cat", "dog", "goat", "cow", "horse"};
	private static final String[] refrain = { "",
			"It wriggled and jiggled and tickled inside her.\n",
			"How absurd to swallow a bird!\n",
			"Imagine that, to swallow a cat!\n",
			"What a hog, to swallow a dog!\n",
			"Just opened her throat and swallowed a goat!\n",
			"I don't know how she swallowed a cow!\n",
			"She's dead, of course!"
	};

	String verse(int v) {
		if (v == 8)
			return "I know an old lady who swallowed a horse.\nShe's dead, of course!";

		v--; // correct zero-index misalignment

		// always start with "I know an old lady who swallowed a ...", and then a specific refrain
		String remit = String.format("I know an old lady who swallowed a %s.\n%s", chain[v], refrain[v]);

		// after the specific refrain, iterate through the whole chain of animals
		while (v-- > 0)
			if (v == 1) // the "wriggle and jiggle and tickling spider" is kind of an anomalous line
				remit += "She swallowed the bird to catch the spider that wriggled and jiggled and tickled inside her.\n";
			else
				remit += String.format("She swallowed the %s to catch the %s.\n", chain[v+1], chain[v]);

		// end with the outtro "I don't know why..."
		return remit + "I don't know why she swallowed the fly. Perhaps she'll die.";
	}

	String verses(int start, int end) {
		String remit = "";

		while (start++ <= end)
			remit += verse(start - 1) + "\n\n";

		return remit.trim();
	}

	String wholeSong() {
		return verses(1, 8);
	}

}