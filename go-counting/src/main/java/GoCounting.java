import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

class GoCounting {
	private Player[][] boardMatrix;
	private Set<Point> alreadyTested;
	private Player owner = Player.NONE;
	private boolean territoryIsValid;

	GoCounting(String input) {
		String[] lines = input.split("\n");

		boardMatrix = new Player[lines.length][lines[0].length()];

		for (int i = 0; i < lines.length; i++) {
			boardMatrix[i] = lines[i].chars().mapToObj(cell -> {
				if (cell == (int) 'B') return Player.BLACK;
				if (cell == (int) 'W') return Player.WHITE;
				return Player.NONE;
			}).toArray(Player[]::new);
		}
	}

	Player getTerritoryOwner(int x, int y) {
		// only empty cells can be considered territory
		if (!boardMatrix[y][x].equals(Player.NONE)) return Player.NONE;

		owner = Player.NONE;
		territoryIsValid = true;
		getTerritory(x, y);

		if (!territoryIsValid) return Player.NONE;
		return owner;
	}

	Set<Point> getTerritory(int x, int y) {
		if (x < 0
				|| y < 0
				|| x >= boardMatrix[0].length
				|| y >= boardMatrix.length ) {
			throw new IllegalArgumentException("Invalid coordinate");
		}

		alreadyTested = new HashSet<>();

		return getTerritory(x, y, new HashSet<>());
	}

	// recursively test cardinally adjacent cells
	private Set<Point> getTerritory(int x, int y, Set<Point> territory) {
		Point newPoint = new Point(x, y);

		// don't recur backwards - avoid already tested points
		if (alreadyTested.contains(newPoint)) return territory;
		else alreadyTested.add(newPoint);

		try {
			if (boardMatrix[y][x].equals(Player.NONE)) {
				territory.add(newPoint);
				territory = getTerritory(x + 1, y, territory);
				territory = getTerritory(x - 1, y, territory);
				territory = getTerritory(x, y + 1, territory);
				territory = getTerritory(x, y - 1, territory);
			}
			// record who owns the space
			else if (owner.equals(Player.NONE)) owner = boardMatrix[y][x];

			// encounter of an opponent's piece invalidates territory ownership
			else if (!boardMatrix[y][x].equals(owner)) {
				territoryIsValid = false;
			}
		} catch (ArrayIndexOutOfBoundsException ignored) {}

		return territory;
	}

	HashMap<Player, Set<Point>> getTerritories() {
		Set<Point> blackTerritory = new HashSet<>();
		Set<Point> whiteTerritory = new HashSet<>();
		Set<Point> noneTerritory = new HashSet<>();

		for (int y = 0; y < boardMatrix.length; y++) {
			for (int x = 0; x < boardMatrix[0].length; x++) {
				Player player = getTerritoryOwner(x, y);
				if (!territoryIsValid) break;
				switch (player) {
					case NONE:
//						noneTerritory.add(new Point(x, y));
						break;
					case BLACK: blackTerritory.add(new Point(x, y)); break;
					case WHITE: whiteTerritory.add(new Point(x, y)); break;
				}
			}
		}

		HashMap<Player, Set<Point>> territories = new HashMap<>();
		territories.put(Player.BLACK, blackTerritory);
		territories.put(Player.WHITE, whiteTerritory);
		territories.put(Player.NONE, noneTerritory);

		return territories;
	}
}
