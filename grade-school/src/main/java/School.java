import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

class School {
	private List<Student> roster = new LinkedList<>();

	private static Comparator<Student> byGrade = Comparator.comparing(
			Student::getGrade
	);

	private static Comparator<Student> byName = Comparator.comparing(
			Student::getName
	);

	List<String> roster() {
		return roster.stream()
				.sorted(byGrade.thenComparing(byName))
				.map(student -> student.name)
				.collect(Collectors.toList());
	}

	List<String> grade(int grade) {
		return roster.stream()
				.filter(student -> student.grade == grade)
				.map(student -> student.name)
				.sorted()
				.collect(Collectors.toList());
	}

	void add(String name, int grade) {
		roster.add(new Student(name, grade));
	}

	class Student {
		private String name;
		private int grade;

		Student(String name, int grade) {
			this.name = name;
			this.grade = grade;
		}

		String getName() {
			return name;
		}

		int getGrade() {
			return grade;
		}
	}
}