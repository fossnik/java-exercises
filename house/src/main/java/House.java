public class House {

	private String[] verb = { "foobar",
			"lay in the house that Jack built.",
			"ate",
			"killed",
			"worried",
			"tossed",
			"milked",
			"kissed",
			"married",
			"woke",
			"kept",
			"belonged to"
	};

	private String[] noun = { "FOOBAR",
			"malt",
			"rat",
			"cat",
			"dog",
			"cow with the crumpled horn",
			"maiden all forlorn",
			"man all tattered and torn",
			"priest all shaven and shorn",
			"rooster that crowed in the morn",
			"farmer sowing his corn",
			"horse and the hound and the horn",
	};

	String verse(int v) {
		if (v == 1)
			return "This is the house that Jack built.";

		String remit = "This is the ";

		while (v-- > 2)
			remit += noun[v] + " that " + verb[v] + " the ";

		return remit + noun[v] + " that " + verb[v];
	}

	String verses(int start, int end) {
		String remit = "";

		while (start++ <= end)
			remit += verse(start - 1) + "\n";

		return remit.trim();
	}

	String sing() {
		return verses(1, 12);
	}

}