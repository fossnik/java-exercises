public class Lasagna {
    final static int expectedMinutes = 40;
    /**
     * This method returns how many minutes the lasagna should be in the oven.
     * The lasagna should be in the oven for 40 minutes.
     * @return int - The number of minutes the lasagna should be in the oven.
     */
    public static int expectedMinutesInOven()
    {
        return expectedMinutes;
    }
    
    public static int remainingMinutesInOven(int minutesInOven)
    {
    	return expectedMinutes - minutesInOven;
    }

    public static int preparationTimeInMinutes(int numberOfLayers)
    {
    	return numberOfLayers * 2;
    }

    public static int totalTimeInMinutes(int numberOfLayers, int elapsedBakeTime)
    {
    	int prepTime = preparationTimeInMinutes(numberOfLayers);
    	return elapsedBakeTime + prepTime;
    }
}
