/**
 * This class contains the isLeapYear method, which can determine if a given year
 * is a leap year.
 */
class Leap
{
    /**
     * The isLeapYear method takes a year and returns true it if is a leap year
     *
     * @param year
     * @return
     */
    boolean isLeapYear(int year)
    {
        // whether the year will be a leap
        boolean isLeapYear = false;

        // if the year is divisible into 400, it is a leap year
        if (year % 400 == 0)
        {
            isLeapYear = true;
        }

        // if the year is divisible by 100, but not by 4, it is a leap year
        if (year % 4 == 0)
        {
            if (!(year % 100 == 0))
            {
                isLeapYear = true;
            }

        }

        return isLeapYear;
    }
}