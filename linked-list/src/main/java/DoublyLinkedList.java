public class DoublyLinkedList<T> {

	// defines current node
	private class Node {
		
		// value held by node
		T nodeValue;

		// pointers to adjacent nodes
		Node nodeBefore;
		Node nodeAfter;

		// the value of the node is passed into constructor as generic
		public Node(T input) {
			nodeValue = input;
			nodeBefore = null;
			nodeAfter = null;
		}
	};

	// adjacent nodes - defines present cursor location
	private Node beforeCursor;
	private Node afterCursor;

	public void push(T input) {

		// pushing a new node - defined by input
		Node newNode = new Node(input);

		// if this isn't the first node, then we point to it from the previous
		if (beforeCursor != null) {
			beforeCursor.nodeAfter = newNode;
			newNode.nodeBefore = beforeCursor;
		} else
			afterCursor = newNode;

		// advance cursor
		beforeCursor = newNode;
	}

	public T pop() {

		// store before re-arranging
		T nodeValue = beforeCursor.nodeValue;

		// swap nodes - temporarily store value
		if (beforeCursor.nodeBefore != null) {
			beforeCursor.nodeBefore.nodeAfter = null;
			beforeCursor = beforeCursor.nodeBefore;
		} else
			beforeCursor = null;

		return nodeValue;
	}

	public void unshift(T input) {
		throw new java.lang.UnsupportedOperationException("Not supported yet.");
	}

	public T shift() {

		// store after re-arranging
		T nodeValue = afterCursor.nodeValue;

		// swap nodes - temporarily store value
		if (afterCursor.nodeAfter != null) {
			afterCursor.nodeAfter.nodeAfter = null;
			afterCursor = afterCursor.nodeAfter;
		} else
			afterCursor = null;

		return nodeValue;
	}
}