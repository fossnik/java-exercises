public class LogLevels
{
    /**
     * Method strips away the forward and trailing parts of a log message
     * @param logLine - the incoming log
     * @return - the cleaned up string from the log
     */
    public static String message(String logLine)
    {
        // get the index of the end part
        int endIndex = logLine.lastIndexOf("]:") + 2;

        // get the substring from that part
        String s = logLine.substring(endIndex);

        // strip leading and trailing characters
        s = s.strip();

        // return cleaned up string
        return s;
    }

    /**
     * Send back the log level of the message
     * @param logLine - the incoming message
     * @return - the log level
     */
    public static String logLevel(String logLine)
    {
        // get the index of the end part
        int endIndex = logLine.lastIndexOf("]:");

        // get the substring from that part
        String s = logLine.substring(1, endIndex);

        // get lowercase
        s = s.toLowerCase();

        // return string
        return s;
    }

    /**
     * This method reformats the string to put the logging level at the end of it
     * @param logLine the original log line
     * @return the reformatted string
     */
    public static String reformat(String logLine)
    {
        // gets the log level string
        String logLevel = logLevel(logLine);

        // convert logLevel to lower case
        String lowCaseLogLevel = logLevel.toLowerCase();

        // get the message part
        String message = message(logLine);

        // create the new stringbuilder object
        StringBuilder sb = new StringBuilder();

        // append elements to create the new string
        sb.append(message).append(" (").append(lowCaseLogLevel).append(")");

        // return the reformatted string
        return sb.toString();
    }
}
