class Markdown {

    String parse(String markdown) {

        // newline split - each line becomes one element in the array "lines"
        String[] lines = markdown.split("\n");

        String result = "";

        // activeList keeps track of <li> elements.
        boolean activeList = false;

        String html;
        for (String line : lines) {
            switch (line.charAt(0)) {
                case '#': // this line is a header
                    html = parseHeader(line);
                    break;

                case '*': // this line is a list item
                    html = parseListItem(line);
                    break;

                default:  // this line is anything else
                    html = parseParagraph(line);
            }

            // Line Item
            if (html.startsWith("<li>"))

                // line continues active list
                if (activeList)
                    result += html;

                // start a new list
                else {
                    result += "<ul>" + html;
                    activeList = true;
                }

            // NOT a Line Item
            else

                // just a regular line
                if (!activeList)
                    result += html;

                // terminate an active list
                else {
                    result += "</ul>" + html;
                    activeList = false;
                }

        }

        return activeList ? result + "</ul>" : result;
    }

    // count instances of '#' and set header size accordingly
    private String parseHeader(String markdown) {
        int count = 0;

        for (int i = 0; i < markdown.length() && markdown.charAt(i) == '#'; i++)
            count++;

        String line = markdown.substring(count + 1);

        return String.format("<h%s>%s</h%s>", count, line, count);
    }

    // insert line (everything after '*') between <li> tags
    private String parseListItem(String markdown) {
        return "<li>" + parseBoldText(markdown.substring(2)) + "</li>";
    }

    // insert line between <p> tags
    private String parseParagraph(String markdown) {
        return "<p>" + parseBoldText(markdown) + "</p>";
    }

    // insert html font effects
    private String parseBoldText(String markdown) {
        return markdown
                .replaceAll("__(.+)__", "<strong>$1</strong>")
                .replaceAll("_(.+)_", "<em>$1</em>");
    }
}