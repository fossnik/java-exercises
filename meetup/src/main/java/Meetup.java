import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class Meetup {

	private LocalDate date;

	Meetup(int mm, int yyyy) {
		date = LocalDate.of(yyyy, mm, 1); // first day of month
	}

	LocalDate day(DayOfWeek dayOfWeek, MeetupSchedule meetupSchedule) {
		switch(meetupSchedule) {
			case TEENTH:
				while (date.getDayOfMonth() < 13)
					date = date.with(TemporalAdjusters.next(dayOfWeek));
				break;
			case FIRST:
				return date.with(TemporalAdjusters.firstInMonth(dayOfWeek));
			case SECOND:
				return getNthNamedDayOfMonth(2, dayOfWeek, date);
			case THIRD:
				return getNthNamedDayOfMonth(3, dayOfWeek, date);
			case FOURTH:
				return getNthNamedDayOfMonth(4, dayOfWeek, date);
			case LAST:
				return date.with(TemporalAdjusters.lastInMonth(dayOfWeek));
		}

		return date;
	}

	private LocalDate getNthNamedDayOfMonth(int n, DayOfWeek dayOfWeek, LocalDate dateLocal) {
		// (exception is for dates that are the first day of the month)
		if (n == 0 || n == 1 && date == date.with(TemporalAdjusters.firstInMonth(dayOfWeek)))
			return dateLocal;

		return getNthNamedDayOfMonth(
				n - 1,
				dayOfWeek,
				dateLocal.with(TemporalAdjusters.next(dayOfWeek))
		);
	}

}
