class MicroBlog
{
    public String truncate(String input)
    {
        // get the length of the input string
        int inputLength = input.length();

        // count how many code points are between index 0 and the end of the input string
        int numberOfCodePoints = input.codePointCount(0, inputLength);

        // get the length that the return string will be in codePoints (minimum of 5)
        int codePointOffset = Math.min(5, numberOfCodePoints);

        // get the index within the given char sequence that is offset from the
        // given index by codePointOffset code points
        int offsetByCodePoints = input.offsetByCodePoints(0, codePointOffset);

        // get a truncated string using the calculated offset of codepoints
        String truncatedString = input.substring(0, offsetByCodePoints);

        // return the truncated string
        return truncatedString;
    }
}
