import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class MinesweeperBoard {

	private final List<String> inputBoard;
	private int height;
	private int width;

	MinesweeperBoard(List<String> inputBoard) {
		this.inputBoard = inputBoard;
	}

	List<String> withNumbers() {
		if (inputBoard.equals(Collections.emptyList()))
			return Collections.emptyList();

		if (inputBoard.equals(Collections.singletonList("")))
			return Collections.singletonList("");

		this.height = inputBoard.size();
		this.width = inputBoard.get(0).length();

		String[] remit = new String[this.height]; // will pass back as a list

		char[] outputRow = new char[this.width];  // store rows in processing
		for (int row = 0; row < this.height; row++) {
			for (int col = 0; col < this.width; col++)
				outputRow[col] = countAdjacentMines(row, col);

			remit[row] = String.valueOf(outputRow);
		}

		return Arrays.asList(remit);
	}

	private char countAdjacentMines(int row, int col) {
		if (inputBoard.get(row).charAt(col) == '*') // is mine?
			return '*';

		int count = 0;

		// kooky ternary operators here just prevent out-of-bounds errors
		for (int r = (row - 1 >= 0 ? row - 1 : 0);
			 r <= (row + 1 < this.height ? row + 1 : this.height - 1);
			 r++)
			for (int c = (col - 1 >= 0 ? col - 1 : 0);
				 c <= (col + 1 < this.width ? col + 1 : this.width - 1);
				 c++)
				if (inputBoard.get(r).charAt(c) == '*')
					count++;

		// return blanks instead of zeros
		return count == 0 ? ' ' : Integer.toString(count).charAt(0);
	}
}
