import java.util.Map;
import java.util.stream.Collectors;

class ParallelLetterFrequency {
	private String letters;

	ParallelLetterFrequency(String input) {
		this.letters = input.toLowerCase().replaceAll("[^a-z]", "");
	}

	Map<Integer, Integer> letterCounts() {
		return this.letters
				.chars()
				.parallel()
				.boxed()
				.collect(Collectors.groupingBy(letter -> letter,
						Collectors.summingInt(letter -> 1)));
	}
}