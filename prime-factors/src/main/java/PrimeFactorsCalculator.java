import java.util.ArrayList;
import java.util.List;

class PrimeFactorsCalculator {

	public List<Long> calculatePrimeFactorsOf(long number) {
		List<Long> remit = new ArrayList<Long>();
		long divisor = 2L;

		while (number > 1L) {
			while (number % divisor == 0) {
				remit.add(divisor);
				number = number / divisor;
			}
			divisor++;
		}

		return remit;
	}
}
