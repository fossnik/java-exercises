import java.util.*;
import java.util.stream.Collectors;

class PythagoreanTriplet {

	private int a, b, c;

	private List<int[]> summingNumbers = new ArrayList<>();

	PythagoreanTriplet(int _a, int _b, int _c) {
		this.a = _a;
		this.b = _b;
		this.c = _c;
	}

	private PythagoreanTriplet() { }

	static PythagoreanTriplet makeTripletsList() {
		return new PythagoreanTriplet();
	}

	PythagoreanTriplet withFactorsLessThanOrEqualTo(int i) {
		return this;
	}

	PythagoreanTriplet thatSumTo(int i) {

		for (int a = 0; a <= i; a++)
			for (int b = 0; b <= i; b++)
				for (int c = 0; c <= i; c++) {
					if (a + b > i) // slightly more efficient? :/
						break;
					if (a + b + c == i)
						summingNumbers.add(new int[]{a, b, c});
				}

		return this;
	}

	final List<PythagoreanTriplet> build() {
		return this.summingNumbers.stream().map(n -> {
			if (Math.pow(n[0], 2) * Math.pow(n[1], 2) == Math.pow(n[2], 2)) {
				Arrays.sort(n);
				return n[0] + "," + n[1] + "," + n[2];
			}
			return null;
		}).distinct()
				.filter(Objects::nonNull)
				.map(n -> new PythagoreanTriplet(Integer.valueOf(n.split(",")[0]), Integer.valueOf(n.split(",")[1]), Integer.valueOf(n.split(",")[2])))
				.collect(Collectors.toList());
	}

}