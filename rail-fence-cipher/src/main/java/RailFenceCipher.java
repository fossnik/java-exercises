class RailFenceCipher {
	private int numRails;
	private int rowLen;

	private enum Tack {
		UP, DOWN
	}

	RailFenceCipher(int numRails) {
		this.numRails = numRails;
	}

	String getEncryptedData(String input) {
		rowLen = input.length();

		char[][] dataMatrix = getMatrix(input);

		StringBuilder remit = new StringBuilder();
		for (char[] chars : dataMatrix) {
			for (char c : chars) {
				if (c != '\u0000') remit.append(c);
			}
		}

		return remit.toString();
	}

	String getDecryptedData(String input) {
		rowLen = input.length();

		// get an empty matrix (along valid zig-zag path indicated by '\u0001')
		char pathCharacter = '\u0001';
		char[][] dataMatrix = getMatrix(new String(new char[rowLen]).replaceAll("\\u0000", String.valueOf(pathCharacter)));

		int index = 0;
		for (int y = 0; y < dataMatrix.length; y++) {
			char[] row = dataMatrix[y];
			for (int x = 0; x < row.length; x++) {
				char c = row[x];
				if (c == pathCharacter && index < rowLen) {
					dataMatrix[y][x] = input.charAt(index);
					index++;
				}
			}
		}

		return pingPongPickup(0, 0, Tack.DOWN, "", dataMatrix);
	}

	private char[][] getMatrix(String input) {
		char[][] dataMatrix = new char[numRails][rowLen];
		Tack tack = Tack.DOWN;
		int y = 0, x = 0;
		for (char c : input.toCharArray()) {
			dataMatrix[y][x] = c;

			// reverse Y-axis direction if necessary
			if (tack == Tack.DOWN && y >= numRails - 1) {
				tack = Tack.UP;
			}
			else if (tack == Tack.UP && y <= 0) {
				tack = Tack.DOWN;
			}

			x++;
			y += tack == Tack.DOWN ? 1 : -1;
		}

		return dataMatrix;
	}

	private String pingPongPickup(int x, int y, Tack tack, String remit, char[][] dataMatrix) {
		// base case - end of rail
		if (x == rowLen) return remit;

		remit += dataMatrix[y][x];

		if (tack == Tack.DOWN) {
			if (y < numRails - 1) {
				return pingPongPickup(x + 1, y + 1, Tack.DOWN, remit, dataMatrix);
			} else {
				return pingPongPickup(x + 1, y - 1, Tack.UP, remit, dataMatrix);
			}
		} else {
			if (y > 0) {
				return pingPongPickup(x + 1, y - 1, Tack.UP, remit, dataMatrix);
			} else {
				return pingPongPickup(x + 1, y + 1, Tack.DOWN, remit, dataMatrix);
			}
		}
	}
}
