/**
 * This is a program for translating the colors from resistor bands into number values
 */

/**
 * @author Zack H
 * This class translates the color into a number value
 */
class ResistorColorDuo
{
    /**
     * This method takes a single color and returns the value
     * @param color
     * @return the integer value of the color
     */
    int getColorValue(String color)
    {
        // the value of the color
        int value = -1;

        // set value to the correct value for the color
        switch (color)
        {
            case "black":
                value = 0;
                break;
            case "brown":
                value = 1;
                break;
            case "red":
                value = 2;
                break;
            case "orange":
                value = 3;
                break;
            case "yellow":
                value = 4;
                break;
            case "green":
                value = 5;
                break;
            case "blue":
                value = 6;
                break;
            case "violet":
                value = 7;
                break;
            case "grey":
                value = 8;
                break;
            case "white":
                value = 9;
                break;
            default:
                System.out.println("Invalid Color!");

        } // end of switch

        // return the numerical value of the color
        return value;

    } // end of method

    /**
     * This method takes an array of colors and translates the first 2
     * @param colors - an array of color values
     * @return the value of the colors array as integer
     */
    int value(String[] colors)
    {
        // build output as a string
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 2; i++)
        {
            // get the index from the array of colors
            String color = colors[i];

            // translate it into an integer value
            int value = getColorValue(color);

            // append the integer value as a string
            sb.append(value);

        } // end of loop

        // parse the string into an integer
        int value = Integer.parseInt(sb.toString());

        // return the value
        return value;

    } // end of method

} // end of class
