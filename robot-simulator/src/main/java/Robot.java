public class Robot {

	private GridPosition gridPosition;
	private Orientation orientation;

	public Robot(GridPosition initialGridPosition, Orientation initialOrientation) {
		this.gridPosition = initialGridPosition;
		this.orientation = initialOrientation;
	}

	public void simulate(String input) {
		for (char command : input.toCharArray())
			switch (command) {
				case 'A': advance(); break;
				case 'L': turnLeft(); break;
				case 'R': turnRight(); break;
			}
	}

	public void advance() {
		switch (this.orientation) {
			case NORTH:
				this.gridPosition = new GridPosition(this.gridPosition.x, this.gridPosition.y + 1); break;
			case SOUTH:
				this.gridPosition = new GridPosition(this.gridPosition.x, this.gridPosition.y - 1); break;
			case EAST:
				this.gridPosition = new GridPosition(this.gridPosition.x + 1, this.gridPosition.y); break;
			case WEST:
				this.gridPosition = new GridPosition(this.gridPosition.x - 1, this.gridPosition.y); break;
		}
	}

	public void turnLeft() {
		switch (this.orientation) {
			case NORTH: this.orientation = Orientation.WEST; break;
			case SOUTH: this.orientation = Orientation.EAST; break;
			case EAST: this.orientation = Orientation.NORTH; break;
			case WEST: this.orientation = Orientation.SOUTH; break;
		}
	}

	public void turnRight() {
		switch (this.orientation) {
			case NORTH: this.orientation = Orientation.EAST; break;
			case SOUTH: this.orientation = Orientation.WEST; break;
			case EAST: this.orientation = Orientation.SOUTH; break;
			case WEST: this.orientation = Orientation.NORTH; break;
		}
	}

	public Orientation getOrientation() {
		return this.orientation;
	}

	public GridPosition getGridPosition() {
		return this.gridPosition;
	}
}
