/**
 * An exercise
 */

/**
 * The Say
 * @param number the decimal number to convert to English
 * @return String an English spoken-word of the number
 */
public class Say
{

    public String say(long number)
    {
        throw new UnsupportedOperationException("Delete this statement and write your own implementation.");
    }
}
