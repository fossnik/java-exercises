import java.util.stream.Stream;

class Cipher {
	private String key;

	Cipher() {
		this.key = generateKey();
	}

	Cipher(String providedKey) {
		if (providedKey.length() < 1) {
			throw new IllegalArgumentException("Key must be at least 1 character");
		}

		if (!providedKey.matches("\\p{javaLowerCase}*")	) {
			throw new IllegalArgumentException("Key must be all lowercase letters");
		}

		this.key = providedKey;
	}

	String getKey() {
		return this.key;
	}

	String encode(String input) {
		StringBuilder sb = new StringBuilder();

		int keyIndex = 0;
		for (int plainChar : input.codePoints().toArray()) {
			int cipherChar = key.codePointAt(keyIndex);

			sb.append((char) ((plainChar - 97 + cipherChar - 97) % 26 + 97));

			// advance key index (not beyond length of key)
			keyIndex++;
			keyIndex %= this.key.length();
		}

		return sb.toString();
	}

	String decode(String input) {
		StringBuilder sb = new StringBuilder();

		int keyIndex = 0;
		for (int plainChar : input.codePoints().toArray()) {
			int cipherChar = key.codePointAt(keyIndex);

			sb.append((char) ((((plainChar - 97) - (cipherChar - 97)) + 26) % 26 + 97));

			// advance key index (not beyond length of key)
			keyIndex++;
			keyIndex %= this.key.length();
		}

		return sb.toString();
	}

	private String generateKey() {
		return Stream.generate(Math::random)
				.limit(100)
				.map(l -> (char) (l * 26 + 97))
				.collect(StringBuilder::new,
						(sb, i) -> sb.append((char) i),
						StringBuilder::append)
				.toString();
	}
}