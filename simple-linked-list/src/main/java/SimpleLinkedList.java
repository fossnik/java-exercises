import java.util.Arrays;
import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;

class SimpleLinkedList<T> {

	private Deque<T> list = new LinkedBlockingDeque<>();

	SimpleLinkedList(T[] values) {
		Arrays.stream(values).forEach(value -> list.push(value));
	}

	SimpleLinkedList() {
		list = new LinkedBlockingDeque<>();
	}

	int size() {
		return list.size();
	}

	T pop() {
		return list.pop();
	}

	void push(T i) {
		list.push(i);
	}

	void reverse() {
		Deque<T> temp = new LinkedBlockingDeque<>();

		for (T t : list) {
			temp.addFirst(t);
		}

		list = temp;
	}

	Object[] asArray(Object ignored) {
		return Arrays.stream(list.toArray()).toArray();
	}
}
