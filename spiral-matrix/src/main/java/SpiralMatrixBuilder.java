class SpiralMatrixBuilder {
	private enum Direction {
		EAST(1, 0),
		SOUTH(0, 1),
		WEST(-1, 0),
		NORTH(0, -1);

		private final int x, y;

		Direction(int _x, int _y) {
			this.x = _x;
			this.y = _y;
		}

		public void setNext() {
			v = Direction.values()[(v.ordinal() + 1) % 4];
		}
	}

	private static Direction v = Direction.EAST;

	int[][] buildMatrixOfSize(int edgeSize) {
		int[][] remit = new int[edgeSize][edgeSize];

		for (
				int i = 1, x = -1, y = 0;
				i <= Math.pow(edgeSize, 2);
			)
		{
			if (v.x != 0) {
				if (x + v.x < 0 || x + v.x > edgeSize - 1 || remit[y][x + v.x] != 0) {
					v.setNext();
					continue;
				} else {
					x += v.x;
				}
			} else {
				if (y + v.y < 0 || y + v.y > edgeSize - 1 || remit[y + v.y][x] != 0) {
					v.setNext();
					continue;
				} else {
					y += v.y;
				}
			}

			remit[y][x] = i++;
		}

		return remit;
	}
}
