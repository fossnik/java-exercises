import java.util.ArrayList;
import java.util.List;

class RelationshipComputer<T> {
	Boolean isSublist(List listA, List listB) {
		if (listA.toArray().length == 0)
			return true;

		// for indexes of potential subarray heads
		List<Integer> potentialListHeads = new ArrayList<>();

		// items in list A that match list B's head are potential sublists
		for (int i = 0; i < listB.size(); i++)
			if (listB.get(i) == listA.get(0))
				potentialListHeads.add(i);

		// comb through each list head potential - search for complete sublist
		for (int headIndex : potentialListHeads)
			try {
				if (listA.equals(listB.subList(headIndex, headIndex + listA.size())))
					return true;
			} catch (IndexOutOfBoundsException ignored) {}

		return false;
	}

	Relationship computeRelationship(List listA, List listB) {
		return listA.equals(listB)
				? Relationship.EQUAL
				: isSublist(listA, listB)
					? Relationship.SUBLIST
					: isSublist(listB, listA)
						? Relationship.SUPERLIST
						: Relationship.UNEQUAL;
	}
}
