import java.util.*;
import java.util.stream.Collectors;

class TeamRecord {
	private int Wins, Losses, Draws;

	TeamRecord() {
		this.Draws = 0;
		this.Losses = 0;
		this.Wins = 0;
	}

	public int getMatchesPlayed() {
		return this.Draws + this.Wins + this.Losses;
	}

	public int getPoints() {
		return this.Draws + this.Wins * 3;
	}

	public int getWins() {
		return Wins;
	}

	public void addWin() {
		Wins++;
	}

	public int getLosses() {
		return Losses;
	}

	public void addLoss() {
		Losses++;
	}

	public int getDraws() {
		return Draws;
	}

	public void addDraw() {
		Draws++;
	}
}

class Team extends TeamRecord {
	private String name;

	Team(String _name) {
		super();
		this.name = _name;
	}

	String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		// left pad 31 characters
		return String.format("%-31s|  %d |  %d |  %d |  %d |  %d",
					this.getName(),
					this.getMatchesPlayed(),
					this.getWins(),
					this.getDraws(),
					this.getLosses(),
					this.getPoints()
				);
	}
}

class Tournament {
	private Map<String, Team> teams = new HashMap<>();

	private static Comparator<Team> byPoints = Comparator.comparing(
			TeamRecord::getPoints, Comparator.reverseOrder()
	);

	private static Comparator<Team> byName = Comparator.comparing(
			Team::getName
	);

	String printTable() {
		String remit = "Team                           | MP |  W |  D |  L |  P\n";

		return remit.concat(teams.values().stream()
						.sorted(byPoints.thenComparing(byName))
						.map(Team::toString)
						.collect(Collectors.joining("\n"))).trim() + "\n";
	}

	void applyResults(String input) {
		Arrays.stream(input.split("\n"))
			.forEach(line -> {
				String[] s = line.split(";");
				String team1 = s[0];
				String team2 = s[1];
				String outcome = s[2];

				// create team if it doesn't exist already
				for (String teamName : new String[]{team1, team2}) {
					if (teams.values().stream().noneMatch(team ->
							team.getName().equals(teamName)))
					{
						teams.put(teamName, new Team(teamName));
					}
				}

				if (outcome.equals("win")) {
					teams.get(team1).addWin();
					teams.get(team2).addLoss();
				}
				else if (outcome.equals("loss")) {
					teams.get(team1).addLoss();
					teams.get(team2).addWin();
				}
				else {
					teams.get(team1).addDraw();
					teams.get(team2).addDraw();
				}
			});
	}
}
