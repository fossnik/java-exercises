class TwoBucket {

	private String finalBucket;
	private int buck2fill;
	private int moves = 1;

	TwoBucket(int buck1size, int buck2size, int desiredLiters, String firstFilled) {
		int levelA = firstFilled.equals("one") ? buck1size : 0;
		int levelB = firstFilled.equals("two") ? buck2size : 0;

		while (levelA != desiredLiters && levelB != desiredLiters) {
			moves++;

			if (buck1size == desiredLiters)
			{
				levelA = buck1size;
			}
			else if (buck2size == desiredLiters)
			{
				levelB = buck2size;
			}
			else if (firstFilled.equals("one"))
			{
				if (levelA == 0)
				{
					levelA = buck1size;
				}
				else
				{
					int temp = buck2size - levelB;
					if (temp == 0)
					{
						levelB = 0;
					}
					else if (levelA >= temp)
					{
						levelA -= temp;
						levelB = buck2size;
					}
					else // (temp > buckA)
					{
						levelB += levelA;
						levelA = 0;
					}
				}
			}
			else // firstFilled.equals("two")
			{
				if (levelB == 0)
				{
					levelB = buck2size;
				}
				else
				{
					int temp = buck1size - levelA;
					if (temp == 0)
					{
						levelA = 0;
					}
					else if (levelB >= temp)
					{
						levelB -= temp;
						levelA = buck1size;
					}
					else // (temp > buckB)
					{
						levelA += levelB;
						levelB = 0;
					}
				}
			}
		}

		this.finalBucket = (levelA == desiredLiters) ? "one" : "two";
		this.buck2fill = (levelA == desiredLiters) ? levelB : levelA;
	}

	int getTotalMoves() {
		return moves;
	}

	// returns which bucket is filled last
	Object getFinalBucket() {
		return finalBucket;
	}

	// returns amount remaining
	Object getOtherBucket() {
		return buck2fill;
	}

}