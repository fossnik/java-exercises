import java.util.*;

class WordSearcher {

	private int yMax;
	private int xMax;
	private char[][] searchMatrix;

	Map<String, Optional<WordLocation>> search(Set<String> searchWords, char[][] searchMatrix) {
		Map<String, Optional<WordLocation>> wordLocations = new HashMap<>();
		this.yMax = searchMatrix.length;
		this.xMax = searchMatrix[0].length;
		this.searchMatrix = searchMatrix;

		for (String word : searchWords) {
			// identify all locations of the word's initial letter
			List<Pair> firstLetterPairs = findLocationsOfInitialLetter(word.charAt(0));

			// map the first pair to multiple adjacent second pairs (one to many)
			Map<Pair, List<Pair>> firstAndSecondPairsOtm = findSecondLetter(word.charAt(1), firstLetterPairs);

			// try to match subsequent letters along the same axis
			WordLocation wordLocation = checkForWord(firstAndSecondPairsOtm, word);

			// append word location if found, otherwise append empty value
			wordLocations.put(word, wordLocation != null ? Optional.of(wordLocation) : Optional.empty());
		}

		return wordLocations;
	}

	// if the sought word starts with 'a' - find all instances of 'a'
	private List<Pair> findLocationsOfInitialLetter(char matchLetter) {
		List<Pair> locations = new LinkedList<>();

		for (int y = 0; y < this.yMax; y++)
			for (int x = 0; x < this.xMax; x++)
				if (this.searchMatrix[y][x] == matchLetter)
					locations.add(new Pair(x, y));

		return locations;
	}

	// search adjacent squares for a correct second letter adjoining the first
	private Map<Pair, List<Pair>> findSecondLetter(char matchLetter, List<Pair> firstLetterPairs) {
		Map<Pair, List<Pair>> firstAndSecondPairsOtm = new HashMap<>();
		for (Pair p : firstLetterPairs) {
			List<Pair> secondLetterPairs = new LinkedList<>();

			int pY = p.getY(), pX = p.getX();
			for (int y = (pY - 1 >= 0 ? pY - 1 : 0);
				 y <= (pY + 1 < this.yMax ? pY + 1 : this.yMax - 1); y++)
				for (int x = (pX - 1 >= 0 ? pX - 1 : 0);
					 x <= (pX + 1 < this.xMax ? pX + 1 : this.xMax - 1); x++)
					if (this.searchMatrix[y][x] == matchLetter &&
							!(pX == x && pY == y)) // don't self-match
						secondLetterPairs.add(new Pair(x, y));

			if (secondLetterPairs.size() > 0)
				firstAndSecondPairsOtm.put(p, secondLetterPairs);
		}

		return firstAndSecondPairsOtm;
	}

	private WordLocation checkForWord(Map<Pair, List<Pair>> firstAndSecondPairsOtm, String word) {

		// loop through each outer (first letter) pair
		for (Map.Entry<Pair, List<Pair>> entry : firstAndSecondPairsOtm.entrySet()) {
			Pair firstLetter = entry.getKey();
			List<Pair> seconds = entry.getValue();

			// loop through each inner (second letter) pair
			for (Pair secondLetter : seconds) {
				int x = secondLetter.getX();
				int y = secondLetter.getY();

				// change between co-ordinates first and second
				int xDelta = x - firstLetter.getX();
				int yDelta = y - firstLetter.getY();

				// reject word that exceeds boundary along expansion axis
				if (x + xDelta * word.length() > this.xMax + 1 ||
						y + yDelta * word.length() > this.yMax + 1 ||
						x + xDelta * word.length() < -2 ||
						y + yDelta * word.length() < -2)
					continue;

				// loop through each letter in word to verify match
				boolean isMatch = true;
				for (int i = 2; i < word.length(); i++)
					if (this.searchMatrix[y += yDelta][x += xDelta] != word.charAt(i))
						isMatch = false;

				// if it's a match then add the word location
				if (isMatch)
					return new WordLocation(
							new Pair(firstLetter.getX() + 1, // Start co-ordinate
									firstLetter.getY() + 1),
							new Pair(x + 1,                  // End co-ordinate
									y + 1)
					);
			}
		}

		return null;
	}
}
