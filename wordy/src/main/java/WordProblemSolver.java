import java.util.Arrays;
import java.util.Iterator;

public class WordProblemSolver {

	public int solve(String input) {

		// cut out the "what is" and "by" and "?" - populate an array - avail an iterator
		Iterator<String> elements = Arrays.asList(input.replace("What is ", "").replace("?","").replaceAll(" by ", " ").split(" ")).iterator();

		try {
			int total = Integer.parseInt(elements.next());

			while (elements.hasNext())
				switch (elements.next()) { // get the mathematical operator
					case "plus":
						total += Integer.parseInt(elements.next());
						break;
					case "minus":
						total -= Integer.parseInt(elements.next());
						break;
					case "divided":
						total /= Integer.parseInt(elements.next());
						break;
					case "multiplied":
						total *= Integer.parseInt(elements.next());
						break;
					default:
						throw new IllegalArgumentException("I'm sorry, I don't understand the question!");
				}

			return total;

		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("I'm sorry, I don't understand the question!");
		}
	}

}
