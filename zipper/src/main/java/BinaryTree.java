import java.util.Objects;

class BinaryTree<T> {

	private final Zipper<T> root;

	BinaryTree(Zipper<T> root) {
		this.root = root;
	}

	BinaryTree(T value) {
		this(new Zipper<>(value));
	}

	Zipper<T> getRoot() {
		return this.root;
	}

	String printTree() {
		return root.printTree();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		BinaryTree that = (BinaryTree) o;
		return Objects.equals(root, that.root);
	}
}
