class Zipper<T> {

	private T value;
	Zipper<T> left, right, up;

	Zipper(T value) {
		this.value = value;
	}

	void setLeft(Zipper<T> left) {
		this.left = left;

		if (left != null) left.up = this;
	}

	void setRight(Zipper<T> right) {
		this.right = right;

		if (right != null) right.up = this;
	}

	void setValue(T value) {
		this.value = value;
	}

	T getValue() {
		return this.value;
	}

	BinaryTree toTree() {
		Zipper<T> root = this;

		// traverse to the top
		while (root.up != null) root = root.up;

		return new BinaryTree<>(root);
	}

	String printTree() {
		return String.format("value: %s, left: %s, right: %s", value, left, right);
	}

	@Override
	public String toString() {
		return String.format("{ value: %s, left: %s, right: %s }", value, left, right);
	}
}
